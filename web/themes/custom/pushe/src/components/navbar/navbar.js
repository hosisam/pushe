(function ($, Drupal) {

  Drupal.behaviors.navbarToggle = {
    attach: function (context, settings) {
      
      //  Navbar toggle
      const toggleNav = function ($this) {
        $this.toggleClass("collapsed active");
        $("body").toggleClass("nav-open");
      };

      // Navbar Toggle
      $('.navbar-toggler').click(function () {
        toggleNav($(this));
      });
    }
  };
})(jQuery, Drupal);
