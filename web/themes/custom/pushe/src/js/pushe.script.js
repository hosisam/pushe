import 'popper.js';
import 'bootstrap';


// Components
import "../components/navbar/navbar.js";


(function ($, drupal) {

  // changes text in hero block
  Drupal.behaviors.changingText = {
    attach: function attach(context) {

      (function(){
      var text = $('.block--hero i.alter-text').text().trim().split("،");
      i = 0;
      setInterval(function(){
          $('.block--hero strong').fadeIn(function(){
              $(this).html(text[i=(i+1)%text.length]).fadeIn();
          });
      }, 3000);
      });
    }
  };

  // adds slider styles to push notification in front page
  Drupal.behaviors.notificationSlider = {
      attach: function attach(context) {
      
      // makes nutifications slideshow
      var $slider = $('.push-notification__field-paragraph ');
      var $slide = $('.push-notification__field-paragraph >.field__item');
      function slides() {
        return $slider.find($slide);
      } 
      
      // set active classes for first notification
      slides().first().addClass('active'); 
      
      // auto slide 
      setInterval(function () {
        var $i = $slider.find('.active').index();
        slides().eq($i).removeClass('active');
        if (slides().length == $i + 1) $i = -1;
        slides().eq($i + 1).addClass('active').removeClass('pre-active');
        slides().eq($i - 1).addClass('pre-active');
      }, 3500); 

      // creates front page plans section like a menu 
      $('.block-plans__field-paragraph >.field__item:nth-child(1) .button').addClass('active');
      $('.block-plans__field-paragraph >.field__item:nth-child(1) .plan__field-features').addClass('active');
      $('.block-plans__field-paragraph >.field__item').click(function () {
        $(this).find('.button').addClass('active');
        $(this).find('.plan__field-features').addClass('active');
        $(this).siblings().find('.button').removeClass('active');
        $(this).siblings().find('.plan__field-features').removeClass('active');
      }); 



      $('.block--code-sample .os__field-features > div.field__item').click(function(){
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
      });
      
    }
  }; 

  // creates sample code block
  Drupal.behaviors.codeSample = {
    attach: function attach(context) {

      // creates sample code block
      $('.os__field-plain-text').wrapAll("<div class='buttoms' />");
      $('.block--code-sample .os__field-plain-text:nth-child(1)').addClass('active');
      $('.block--code-sample .samples:nth-last-child(2)').addClass('active');
      $('.samples:nth-last-child(2) .os__field-features > div.field__item:nth-last-child(1)').addClass('active');
      $('.os__field-plain-text').click(function(){
        var that = this;
        var title = $(this).text();
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
        $(this).parent().siblings().removeClass('active');
        $(this).parent().siblings().find('.active').removeClass('active');
        $('.'+title).addClass('active');
        $('.'+title).find('.os__field-features > div.field__item:nth-last-child(1)').addClass('active');
      })
    }
  };
  
  // adds active class to special menu item on hover
  Drupal.behaviors.specialMenu = {
    attach: function attach(context) {

      // adds active class to special menu item on hover
      $('.navbar .nav-item.special-item').hover(function () {
        $(this).toggleClass('active');
      }); 
    }
  };

  // adds animation to svg flis  
  Drupal.behaviors.svgAnimations = {
    attach: function attach(context) {
      
      // adds svg animation to ipad section
      if ($('.block--beananalystimage').length) {
        $('.block--beananalystimage').waypoint(function() {
          anime({
            targets: 'polyline',
            strokeDashoffset: [anime.setDashoffset, 0],
            easing: 'easeOutExpo',
            duration: 1900,
            delay: function delay(el, i) {
              return i * 1000;
            }
          });
          
          anime({
            targets: '.dots path#Path',
            opacity: 1,
            easing: 'easeInOutBack',
            duration: 1900,
            delay: function delay(el, i) {
              return i * 70;
            }
          });
          waypoint.destroy();
        }, {
          offset: '100%'
        });
      }

      // front page hero animations
      // adds animation to notification
      var tlNotif1 = anime.timeline({
        targets: '#notification1 path',
        easing: 'easeOutExpo',
        duration: 750,
        delay: 1000,
        loop: true
      });
      
      tlNotif1
      .add({
        translateY: -15,
        opacity: 1,
      }, 800)

      .add({
        translateX: -40,
        translateY: -15,
        opacity: 0,
        duration: 400,
      }, 3000);

      var tlNotif2 = anime.timeline({
        targets: '#notification2 path',
        easing: 'easeOutExpo',
        duration: 750,
        delay: 1100,
        loop: true
      });
      
      tlNotif2
      .add({
        translateY: -15,
        opacity: 1,
      }, 800)

      .add({
        translateX: -40,
        translateY: -15,
        opacity: 0,
        duration: 400,
      }, 3000);

      var tlNotif3 = anime.timeline({
        targets: '#notification3 path',
        easing: 'easeOutExpo',
        duration: 750,
        delay: 1100,
        loop: true
      });
      
      tlNotif3
      .add({
        translateY: -15,
        opacity: 1,
      }, 800)

      .add({
        translateX: -40,
        translateY: -15,
        opacity: 0,
        duration: 400,
      }, 3000);
      

      // adds animation to text leyers
      anime({
        targets: '#text path',
        opacity: 1,
        easing: 'easeOutExpo',
        duration: 3000,
        loop: true,
        delay: function delay(el, i) {
          return i * 100;
        }
      });

      // adds animation to location icon
      anime({
        targets: '#location path',
        translateY: -2,
        direction: 'alternate',
        loop: true,
        easing: 'easeInOutQuad',
        autoplay: true
      });

      // adds animation to clouds
      anime({
        targets: '#cloud1',
        translateX: 10,
        direction: 'alternate',
        loop: true,
        easing: 'easeInOutQuad',
        duration: 8000,
        autoplay: true
      });

      anime({
        targets: '#cloud2',
        translateX: -8,
        direction: 'alternate',
        loop: true,
        easing: 'easeInOutQuad',
        duration: 8000,
        autoplay: true
      });

      // adds animation percentage block
      if ($('.block--percentage').length) {
        $('.block--percentage').waypoint(function() {
          
          var number = $('.block--percentage .percentage__field-plain-text').text();
          number= parseInt(number);
          anime({
            targets: '#blue',
            strokeDashoffset: [anime.setDashoffset, number],
            easing: 'cubicBezier(.5, .05, .1, .3)',
            duration: 1900,
            delay: function delay(el, i) {
              return i * 70;
            }
          });
          var roundLogEl = document.querySelector('.percentage__field-plain-text');
          anime({
            targets: roundLogEl,
            innerHTML: [0, number],
            easing: 'cubicBezier(.5, .05, .1, .3)',
            duration: 1900,
            round: 10 
          });
          waypoint.destroy();
        }, {
          offset: '100%'
        });
      }
    }
  };

  // adds indicator to article nodes
  Drupal.behaviors.indicator = {
    attach: function attach(context) {

      if ($('body').hasClass('node-type-article')) {        
        var wh = window.innerHeight,
        dh = document.body.clientHeight,
        dw = document.body.clientWidth,
        b = document.querySelector('.indicator');
  
        window.onscroll = function () {
          var y = window.scrollY,
              wUnit = wh/100,
              hUnit = (dh-wh)/100;
  
          b.style.width = y/hUnit+"%";
        };
      }
    }
  };

  // add headroom to menu 
  Drupal.behaviors.headroomJS = {
    attach: function attach(context) {
      if (!$('body').hasClass('node-type-article')) { 
        var myElement = document.querySelector("nav.navbar");
        // construct an instance of Headroom, passing the element
        var headroom  = new Headroom(myElement);
        // initialise
        headroom.init(); 
      }
    }
  };

  // article nodes navbar
  Drupal.behaviors.articleNav = {
    attach: function attach(context) {
      if ($('body').hasClass('node-type-article')) { 

        $(window).scroll(function(){
          if ($(this).scrollTop() > 20) {
            $('nav.navbar').addClass('slideUp');
          } else {
            $('nav.navbar').removeClass('slideUp');
          }

          if ($(this).scrollTop() > 500) {
            $('nav.navbar').addClass('slideDown');
            $('nav.navbar').removeClass('slideUp');
          } else {
            $('nav.navbar').removeClass('slideDown');
          }
       });
      }
    }
  };

  // comment placeholder
  Drupal.behaviors.addPlaceholder = {
    attach: function attach(context) {
      $('.comment-form .form-item').each(function(){
        var title = $(this).find('label').text();
        $(this).find('input').attr('placeholder' ,  title );
        $(this).find('textarea').attr('placeholder' ,  title );
      });
    }
  };

  // adds slick to client section
  Drupal.behaviors.clientSlick = {
    attach: function attach(context) {
      $('.block--clients .views__content').slick({
        autoplay: true,
        slidesToShow: 6,
        slidesToScroll: 6,
        arrows: false,
        dots: true,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }]
      });
    }
  };

  // adds slick to testimonial block
  Drupal.behaviors.testimonialSlick = {
    attach: function attach(context) {
      $('.block-testimonials__field-paragraph').slick({
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        rtl: true,
        fade: true
      });
    }
  };
  
  // text & content generation
  Drupal.behaviors.addText = {
    attach: function attach(context) {
      $('.comment-form input').attr('placeholder' , 'اسمتون');
    }
  };

  // change timeline image
  Drupal.behaviors.changeTimeline = {
    attach: function attach(context) {
      var windowsize = $(window).width();
      if (windowsize < 769) { 
        $('.block--timeline img').attr('src' , '/sites/default/files/1398-05/timeline-small.png');
      }
    }
  };

  

})(jQuery, Drupal);
