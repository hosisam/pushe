<?php
$update_free_access = TRUE;
$settings['hash_salt'] = 'dflkgjskld4534jnsdfj32lsmfdksdfkjssdf8flkggjdlkfj34';

// Load services definition file.
$settings['container_yamls'][] = __DIR__ . '/services.yml';

// Verbose logging by design
$config['system.logging']['error_level'] = 'verbose';
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
$settings['extension_discovery_scan_tests'] = FALSE;
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
  '.sass-cache',
];
$settings['rebuild_access'] = TRUE;
$settings['skip_permissions_hardening'] = TRUE;

// base url of local environment
$base_url = 'https://webkadeh.ca';  // NO trailing slash!

$databases['default']['default'] = array (
  'database' => 'pushe_db',
  'username' => 'root',
  'password' => '*******',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
